const layout = require('../layout');
const { getError } = require('../../helpers');

module.exports = ({ req, errors }) => {
  return layout({
    content: `
    <main class="d-flex align-items-center min-vh-100 py-3 py-md-0">
    <div class="container">
      <div class="card login-card">
        <div class="row no-gutters">
          <div class="col-md-5">
            <img src="/images/login.jpg" alt="login" class="login-card-img">
          </div>
          <div class="col-md-7">
            <div class="card-body">
              <div class="brand-wrapper">
                <img src="/images/brand.jpg" alt="logo" class="logo">
              </div>
              <p class="login-card-description">Create an account</p>
              <form method="POST">
              <div class="form-group">
              <label class="label">Email</label>
              <input required class="form-control" placeholder="Email" name="email" />
              <p class="help is-danger">${getError(errors, 'email')}</p>
            </div>
            <div class="form-group">
              <label class="label">Password</label>
              <input required class="form-control" placeholder="Password" name="password" type="password" />
              <p class="help is-danger">${getError(errors, 'password')}</p>
            </div>
            <div class="form-group">
              <label class="label">Password Confirmation</label>
              <input required class="form-control" placeholder="Password Confirmation" name="passwordConfirmation" type="password" />
              <p class="help is-danger">${getError(
                errors,
                'passwordConfirmation'
              )}</p>
            </div>
            <button class="login-btn">Sign Up</button>
                </form>
                <p class="login-card-footer-text">Have an account? <a href="/signin" class="text-reset">Sign in</a></p>
            </div>
          </div>
        </div>
      </div>
    </div>
  </main>
  <script src="https://code.jquery.com/jquery-3.4.1.min.js"></script>
  <script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js"></script>
  <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js"></script>
    `
  });
};
