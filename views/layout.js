module.exports = ({ content }) => {
  return `
<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <title>Shop</title>

  <link href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.11.2/css/all.min.css" rel="stylesheet">
  <link href="/css/main.css" rel="stylesheet">
  <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css"
    integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">


    <link href="https://fonts.googleapis.com/css?family=Josefin+Sans:300, 400,700|Inconsolata:400,700" rel="stylesheet">

    <link rel="stylesheet" href="/css/bootstrap.css">
    <link rel="stylesheet" href="/css/style.css">
</head>

<body>
  <header>
    <!-- Navbar -->
    <nav class="navbar navbar-expand-md navbar-light fixed-top scrolling-navbar">
      <div class="container-fluid">

        <!-- Brand -->
        <a class="navbar-brand" href="/">
          <b>TrendyThrifts</b>
        </a>

        <!-- Collapse button -->
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#basicExampleNav"
          aria-controls="basicExampleNav" aria-expanded="false" aria-label="Toggle navigation">
          <span class="navbar-toggler-icon"></span>
        </button>

        <!-- Links -->
        <div class="collapse navbar-collapse" id="basicExampleNav">

          <!-- Right -->
          <ul class="navbar-nav ml-auto">
            <li class="nav-item">
              <a href="/cart" class="nav-link navbar-link-2 waves-effect">
                <i class="fas fa-shopping-cart pl-0"></i>
              </a>
            </li>
            <li class="nav-item">
              <a href="#showProducts" class="nav-link waves-effect">
                Products
              </a>
            </li>
            <li class="nav-item">
              <a href="/signin" class="nav-link waves-effect">
                Sign in
              </a>
            </li>
            <li class="nav-item pl-2 mb-2 mb-md-0">
              <a href="/signup" type="button"
                class="signupBtn btn btn-md btn-rounded btn-navbar waves-effect waves-light">Sign up</a>
            </li>
          </ul>

        </div>
        <!-- Links -->
      </div>
    </nav>
    <!-- Navbar -->
    <div class="jumbotron jumbotron-image color-grey-light">
      <div class="mask rgba-black-strong d-flex align-items-center h-100">
        <div class="container text-center white-text py-5">
          <h1 class="mb-3">Sustainable. Stylish.</h1>
          <p class="mb-0">We are committed to providing fashion that is affordable and environmentally-friendly</p>
        </div>
      </div>
    </div>
  </header>
  
  <!-- Blogs -->

  <section class="site-section py-sm">
  <div class="container">
    <div class="row">
      <div class="col-md-6">
        <h2 class="mb-4">Our Mission</h2>
      </div>
    </div>
    <div class="row blog-entries">
      <div class="col-md-12 col-lg-8 main-content">
        <div class="row">
          <div class="col-md-6">
            <a href="https://www.sustainyourstyle.org/en/whats-wrong-with-the-fashion-industry?gclid=Cj0KCQjw-af6BRC5ARIsAALPIlV8kER5wByN488n5ed-2XvReJkrM-6SBTvLH-XvNZtxUS-VGY-Q3VsaAhv6EALw_wcB" class="blog-entry" >
              <img src="/images/img_5.jpg" alt="Image placeholder">
              <div class="blog-content-body">
                <div class="post-meta">
                  <span class="author mr-2">TRENDYTHRIFTS</span>&bullet;
                  <span class="mr-2">Dec 15, 2019 </span> &bullet;
                  <span class="ml-2"><span class="fa fa-comments"></span> 215</span>
                </div>
                <h2>The Negative Conequences of Fast Fashion on the Environment</h2>
              </div>
            </a>
          </div>
          <div class="col-md-6">
            <a href="https://www.fashionrevolution.org/usa-blog/why-i-invest-in-ethical-fashion/" class="blog-entry">
              <img src="/images/img_6.jpg" alt="Image placeholder">
              <div class="blog-content-body">
                <div class="post-meta">
                  <span class="author mr-2">TRENDYTHRIFTS</span>&bullet;
                  <span class="mr-2">May 15, 2020 </span> &bullet;
                  <span class="ml-2"><span class="fa fa-comments"></span> 306</span>
                </div>
                <h2>Why Buy Ethically Sourced Clothing?</h2>
              </div>
            </a>
          </div>

        </div>
      </div>

      <!-- END main-content -->

      <div class="col-md-12 col-lg-4 sidebar">
              <div class="sidebar-box">
                <div class="bio text-center">
                  <div class="bio-body">
                    <h2>TRENDYTHRIFTS</h2>
                    <p>We ensure our clothes are only made from ethically-sourced materials. Find all of the latest trends here, and take comfort in knowing you can look stylish while making a postive impact on the environment. Fast fashion ends with us! </p>
                    <p>
                    <div class="input-group mb-3">
                    <input type="text" class="form-control" placeholder="Enter your email" aria-label="Enter your email" aria-describedby="basic-addon2">
                    <div class="input-group-append">
                      <button id="subscribe" class="btn btn-outline-secondary" type="button">Subscribe</button>
                    </div>
                  </div>
                  </p>
                  </div>
                </div>
              </div>
    </div>
  </div>
</section>

  ${content}



  <footer class="site-footer">
        <div class="container">
          <div class="row">
            <div class="col-md-6">
              <h3>About Us</h3>

              <p>We are an online thrift store that is dedicated to ending fast fashion by encouraging reusability and sustainability. Create an account today to sell your own products!</p>
            </div>

            <div class="col-md-6">
              <h3>Social Media</h3>

              <p>
              <a href="https://www.instagram.com/"><i class="fab fa-instagram"></i></a>
              <a href="https://www.facebook.com/"><i class="fab fa-facebook-square"></i></a>
              <a href="https://twitter.com/"><i class="fab fa-twitter"></i></a>
              </p>
            </div>
            

 
              </div>
            </div>
          </div>
        </div>
      </footer>
      <!-- END footer -->

      <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.2.1/js/bootstrap.min.js" integrity="sha384-B0UglyR+jN6CkvvICOB2joaf5I4l3gm9GU6Hc1og6Ls7i6U/mkkaduKaBhlAXv9k" crossorigin="anonymous"></script>
</body>

</html>
`;
};