const layout = require('../layout');

module.exports = ({ products }) => {
  var prodCount = 0;

  // want three products per row
  const renderedProducts = products
    .map(product => {
      prodCount ++;

      // add card-deck class to first card
      if (prodCount === 1) {
        return `
      <div class="card-deck">
        <div class="card" style="width: 18rem;">
          <figure>
            <img class="card-img-top" src="data:image/png;base64, ${product.image}"/>
          </figure>
          <div class="card-body">
            <h3 class="subtitle">${product.title}</h3>
            <h5>$${product.price}</h5>
            <form action="/cart/products" method="POST">
              <input hidden value="${product.id}" name="productId"/>
              <button class="signupBtn btn button has-icon is-inverted">
                  <i class="fa fa-shopping-cart"></i> Add to cart
              </button>
            </form>
          </div>
        </div>
        `;
      }

      if (prodCount === 2) {
      return `

      <div class="card" style="width: 18rem;">
        <figure>
          <img class="card-img-top" src="data:image/png;base64, ${product.image}"/>
        </figure>
        <div class="card-body">
          <h3 class="subtitle">${product.title}</h3>
          <h5>$${product.price}</h5>
          <form action="/cart/products" method="POST">
            <input hidden value="${product.id}" name="productId"/>
            <button class="signupBtn btn button has-icon is-inverted">
                <i class="fa fa-shopping-cart"></i> Add to cart
            </button>
          </form>
        </div>
      </div>
      
      `;
      }

      // last card needs to have closing div for "card-deck"
      if (prodCount === 3) {
        prodCount = 0;
        return `
  
        <div class="card" style="width: 18rem;">
          <figure>
            <img class="card-img-top" src="data:image/png;base64, ${product.image}"/>
          </figure>
          <div class="card-body">
            <h3 class="subtitle">${product.title}</h3>
            <h5>$${product.price}</h5>
            <form action="/cart/products" method="POST">
              <input hidden value="${product.id}" name="productId"/>
              <button class="signupBtn btn button has-icon is-inverted">
                  <i class="fa fa-shopping-cart"></i> Add to cart
              </button>
            </form>
          </div>
        </div>
      </div>
  
        
        `;
        }
    })
    .join('\n');

  return layout({
    content: `
      
      <section id="featured">
        <div class="container">
          <div class="columns">
            <div class="column "></div>
            <div class="column is-four-fifths">
              <div id="showProducts">
                <h2 class="title text-center">Featured Items</h2>
                <div class="columns products">
                  ${renderedProducts}  
                </div>
              </div>
            </div>
            <div class="column "></div>
          </div>
        </div>
      </section>
    `
  });
};

