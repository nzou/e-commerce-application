const express = require('express');
const bodyParser = require('body-parser');
const cookieSession = require('cookie-session');
const authRouter = require('./routes/admin/auth');
const productsRouter = require('./routes/products');
const productsAdminRouter = require('./routes/admin/products');
const cartsRouter = require('./routes/carts');

const app = express();
var PORT = process.env.PORT || 3000;

// MIDDLEWARES

// make contents of public directory available to the public
app.use(express.static('public'));
app.use(bodyParser.urlencoded({ extended: true }));
app.use(cookieSession({
    keys: ['ajhs234daud7as39dghvd']
}));
app.use(authRouter);
app.use(productsRouter);
app.use(productsAdminRouter);
app.use(cartsRouter);

app.listen(PORT, () => {
    console.log('Listening');
});
