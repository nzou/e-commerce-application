const fs = require('fs');
const crypto = require('crypto');
const util = require('util');
const Repository = require('./repository');

const scrypt = util.promisify(crypto.scrypt);

class UsersRepository extends Repository {
    // create user with the given attributes
    async create(attrs) {
        attrs.id = this.randomId();

        const salt = crypto.randomBytes(8).toString('hex');
        const hashed = await scrypt(attrs.password, salt, 64);

        const records = await this.getAll();
        const record = {
            // overwrite default password in attrs with hash and salted password
            ...attrs,
            password: `${hashed.toString('hex')}.${salt}`
        };

        records.push(record);

        await this.writeAll(records);

        return record;
    }

    // compare password saved in database vs password that user supplied
    async comparePasswords(saved, supplied) {
        // passwords in database are stored in the following format "hashed.salt"
        // split stored password into hash and salt 
        const result = saved.split('.');
        const hashed = result[0];
        const salt = result[1];

        const hashSupplied = await scrypt(supplied, salt, 64);

        return hashed === hashSupplied.toString('hex');
    }
}

// export instance of class
module.exports = new UsersRepository('users.json');
