const express = require('express');
const cartsRepo = require('../repositories/carts');
const productsRepo = require('../repositories/products');
const showCartTemplate = require('../views/carts/show');
const checkoutTemplate = require('../views/carts/checkout');

const router = express.Router();

// post request to add item to cart
router.post('/cart/products', async (req, res) => {
    // check if user has cart associate to session cookie
    let cart;
    if (!req.session.cartId) {
        // create cart and store cart id on req.session.cartId property
        cart = await cartsRepo.create({ items: [] });
        req.session.cartId = cart.id;
    } else {
        // get cart from repository
        cart = await cartsRepo.getOne(req.session.cartId);
    }

    const existingItem = cart.items.find(item => item.id === req.body.productId)

    if (existingItem) {
        // increment quantity and save cart
        existingItem.quantity++;
    } else {
        // add new product id to items array
        cart.items.push({ id: req.body.productId, quantity: 1 });
    }

    await cartsRepo.update(cart.id, {
        items: cart.items
    });

    res.redirect('/cart');
});

// get request to show items in cart
router.get('/cart', async (req, res) => {
    // redirect user to product page if they don't have a cart
    if (!req.session.cartId) {
        return res.redirect('/');
    }

    const cart = await cartsRepo.getOne(req.session.cartId);

    for (let item of cart.items) {
        const product = await productsRepo.getOne(item.id);
        item.product = product;
    }

    res.send(showCartTemplate({ items: cart.items }));
});

// post request to delete item in cart
router.post('/cart/products/delete', async (req, res) => {
    const { itemId } = req.body;
    const cart = await cartsRepo.getOne(req.session.cartId);

    // create array of items that won't be deleted
    const items = cart.items.filter(item => item.id !== itemId);

    await cartsRepo.update(req.session.cartId, { items });

    res.redirect('/cart');
});

// get request to checkout items
router.get('/checkout', async (req, res) => {
    // redirect user to product page if they don't have a cart
    if (!req.session.cartId) {
        return res.redirect('/');
    }

    const cart = await cartsRepo.getOne(req.session.cartId);

    for (let item of cart.items) {
        const product = await productsRepo.getOne(item.id);
        item.product = product;
    }

    res.send(checkoutTemplate({ items: cart.items }));
});

module.exports = router;
