const { validationResult } = require('express-validator');

module.exports = {
    handleErrors(templateFunction, dataCallback) {
        return async (req, res, next) => {
            const errors = validationResult(req);

            if (!errors.isEmpty()) {
                let data = {};
                if (dataCallback) {
                    data = await dataCallback(req);
                }

                return res.send(templateFunction({ errors, ...data  }));
            }

            // call next middleware or invoke actual route handler
            next();
        };
    },
    requireAuth(req, res, next) {
        if (!req.session.userId) {
            return res.redirect('/signin');
        }

        next();
    }

};
