const express = require('express');

const { handleErrors, requireAuth } = require('./middlewares');
const usersRepo = require('../../repositories/users');
const productsRepo = require('../../repositories/products')
const signupTemplate = require('../../views/admin/auth/signup');
const signinTemplate = require('../../views/admin/auth/signin');
const signedInTemplate = require('../../views/admin/auth/signedIn');
const {
  requireEmail,
  requirePassword,
  requireConfirmation,
  requireValidEmail,
  requireValidPassword
} = require('./validators');

const router = express.Router();

router.get('/signup', (req, res) => {
  res.send(signupTemplate({ req }));
});

router.post(
  '/signup',
  [requireEmail, requirePassword, requireConfirmation],
  handleErrors(signupTemplate),
  async (req, res) => {
    const { email, password } = req.body;
    // create user in user repo to represent this person
    const user = await usersRepo.create({ email, password });

    // store user id inside the users cookie
    req.session.userId = user.id;

    res.redirect('/admin/products');
  }
);

router.get('/signout', (req, res) => {
  req.session = null;
  console.log('You are signed out');
  res.redirect('/');
});

router.get('/signin', (req, res) => {
  res.send(signinTemplate({}));
});

router.post(
  '/signin',
  [requireValidEmail, requireValidPassword],
  handleErrors(signinTemplate),
  async (req, res) => {
    const { email } = req.body;

    const user = await usersRepo.getOneBy({ email });

    req.session.userId = user.id;

    res.redirect('/admin/products');
  }
);

router.get('/signedIn', requireAuth, async (req, res) => {
  const products = await productsRepo.getAll();
  res.send(signedInTemplate({ products }));
});


module.exports = router;
